import './App.css'
import Home from './pages/Home'

function App() {
  return <div className='flex justify-center'>
    <Home/>
  </div>
}

export default App
