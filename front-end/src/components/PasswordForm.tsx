import { FormEvent, MouseEvent, useEffect, useRef, useState } from "react";
import { EyeIcon, EyeSlashIcon, ClipboardIcon } from '@heroicons/react/24/outline'
import { CheckCircleIcon } from '@heroicons/react/24/solid'
import { passwordService } from '../services/password.service';

import { Response, Password } from "../services/models";
import Spinner from "./Spinner";
import { AxiosResponse } from "axios";

interface CopyToClipboard {
    field: string,
    value: boolean
}

interface PasswordFormProps {
    idPassword: number | undefined;
    onCancel?: (event: MouseEvent<HTMLButtonElement>) => void;
    onSave?: (password: Password, isUpdate : boolean) => Promise<void>;
}

function PasswordForm(props: PasswordFormProps) {
    const isUpdate: boolean = props.idPassword != undefined;

    const [isSubmit, setIsSubmit] = useState<boolean>(false);
    const [isLoaded, setIsLoaded] = useState<boolean>(true);
    const [password, setPassword] = useState<Password | undefined>(undefined)
    const [isPasswordShown, setIsPasswordShown] = useState<boolean>(false);
    const [copyToClipboard, setCopyToClipboard] = useState<CopyToClipboard | null>(null);

    const formRef = useRef<HTMLFormElement>(null);
    const usernameRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);

    const resetComponent = () => {
        formRef.current!.reset();
        if(isPasswordShown) setIsPasswordShown(false);
        setPassword(undefined);
    }

    const toggleIsPasswordShown = () => {
        setIsPasswordShown(prevState => !prevState);
    };

    const handleCopyToClipboard = (field: string, text: string) => {
        setCopyToClipboard({ field, value: true });
        navigator.clipboard.writeText(text).then(() => {
            const timer = setTimeout(() => {
                setCopyToClipboard({ field, value: false });
                clearTimeout(timer);
            }, 1000);
        }).catch(err => {
            console.error('Failed to copy text: ', err);
        });
    };

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const formDataObject = new FormData(e.target as HTMLFormElement);

        const password: Password = {
            id: formDataObject.get('id') ? parseInt(formDataObject.get('id') as string, 10) : undefined,
            name: formDataObject.get('name') as string,
            username: formDataObject.get('username') as string,
            password: formDataObject.get('password') as string,
        };

        setIsSubmit(true);
        try {
            let result : AxiosResponse<Response<Password>, unknown> | undefined = undefined;

            if (!isUpdate) {
                result = await passwordService.create(password);
            } else {
                result = await passwordService.update(password.id, password);
            }
            if (props?.onSave) await props.onSave(result.data.data, isUpdate);
            resetComponent();
        } catch (err) { /* empty */ } finally {
            setIsSubmit(false);
        }
    }

    const handleCancel = (e: MouseEvent<HTMLButtonElement>) => {
        resetComponent();
        if (props?.onCancel) props.onCancel(e);
    }

    useEffect(() => {
        const findPassword = async (id: number | undefined) => {
            try {
                setIsLoaded(false);
                let _password: Password | undefined = undefined;
                if (id) {
                    const result = await passwordService.getOne(id);
                    _password = result.data.data
                }
                setPassword(_password)
            } catch (err) { /* empty */ }
            finally {
                setIsLoaded(true);
            }
        }

        findPassword(props.idPassword)
    }, [props.idPassword])

    const classNameLabel = "block text-slate-800 dark:text-slate-50";
    const classNameInput = "block w-full text-slate-500 px-2 py-2 bg-transparent border-b border-gray-300 focus:outline-none focus:border-blue-600 dark:text-slate-400";
    const classNameIcon = "size-4 dark:text-slate-100";
    const classNameIconSuccess = "size-4 text-green-600";

    return (
        <div className="relative">
            {!isLoaded && <div role="status" className="absolute w-full h-full flex items-center justify-center bg-slate-100 dark:bg-gray-800 z-10 opacity-80">
                <Spinner className="inline w-14 h-14 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600"/>
                <span className="sr-only">Loading...</span>
            </div>}
            <form className="w-full text-xs px-4 py-2 flex flex-col gap-4 z-0" onSubmit={handleSubmit} ref={formRef}>
                <input type="hidden" name="id" defaultValue={password?.id} />
                <div>
                    <label htmlFor="name" className={classNameLabel}>Nom</label>
                    <input
                        type="text"
                        id="name"
                        name="name"
                        className={classNameInput}
                        defaultValue={password?.name}
                    />
                </div>
                <div>
                    <label htmlFor="username" className={classNameLabel}>Utilisateur</label>
                    <div className="relative">
                        <input
                            type="text"
                            id="username"
                            name="username"
                            className={classNameInput}
                            ref={usernameRef}
                            defaultValue={password?.username}
                        />
                        <button type="button" className="absolute top-0 end-0 bottom-0" onClick={() => handleCopyToClipboard('username', usernameRef.current!.value)}>
                            {((copyToClipboard?.field === 'username' && !copyToClipboard?.value) || copyToClipboard?.field !== 'username') && <ClipboardIcon className={classNameIcon} />}
                            {copyToClipboard?.field === 'username' && copyToClipboard?.value && <CheckCircleIcon className={classNameIconSuccess} />}
                        </button>
                    </div>
                </div>
                <div>
                    <label htmlFor="password" className={classNameLabel}>Mot de passe</label>
                    <div className="relative">
                        <input
                            type={isPasswordShown ? 'text' : 'password'}
                            id="password"
                            name="password"
                            className={classNameInput}
                            ref={passwordRef}
                            defaultValue={password?.password}
                        />
                        <button type="button" className="absolute top-0 end-0 bottom-0 flex gap-1">
                            {!isPasswordShown && <EyeIcon className={classNameIcon} onClick={toggleIsPasswordShown} />}
                            {isPasswordShown && <EyeSlashIcon className={classNameIcon} onClick={toggleIsPasswordShown} />}
                            {((copyToClipboard?.field === 'password' && !copyToClipboard?.value) || copyToClipboard?.field !== 'password') && <ClipboardIcon className={classNameIcon} onClick={() => handleCopyToClipboard('password', passwordRef.current!.value)} />}
                            {copyToClipboard?.field === 'password' && copyToClipboard?.value && <CheckCircleIcon className={classNameIconSuccess} />}
                        </button>
                    </div>
                </div>
                <div className="flex gap-2 justify-end">
                    <button type="submit" className="px-4 py-2 rounded text-slate-50 bg-blue-800 hover:bg-blue-900 flex gap-2 justify-center" disabled={isSubmit}>
                        {isSubmit && <Spinner className="inline w-4 h-4 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600"/> }
                        {!isSubmit && ((isUpdate) ? 'Mettre à jour' : 'Enregistrer')}
                    </button>
                    <button type="submit" onClick={handleCancel} className="px-4 py-2 rounded text-slate-600 bg-gray-200 hover:bg-gray-300 dark:bg-slate-600 dark:hover:bg-slate-700 dark:text-slate-50">Annuler</button>
                </div>
            </form>
        </div>
    );
}

export default PasswordForm;
