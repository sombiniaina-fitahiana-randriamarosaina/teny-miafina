import { useEffect, useState } from "react";
import { Password, PasswordUI } from "../services/models";
import { TrashIcon } from '@heroicons/react/24/outline';
import { passwordService } from '../services/password.service';
import moment from 'moment';
import 'moment/dist/locale/fr';
import Spinner from "./Spinner";
moment.locale('fr');

interface PasswordsListProps {
    passwords : PasswordUI[];
    onSelect? : (password : Password | undefined) => void;
    onDelete? : (password : Password) => void;
}

function PasswordsList(props : PasswordsListProps) {
    const[passwords, setPasswords] = useState<PasswordUI[]>(props.passwords)
    const [isLoaded, setIsLoaded] = useState(true);

    const handleDelete = async (password : Password)=>{
        try{
            setIsLoaded(false);
            await passwordService.delete(password!.id);
            if(props?.onDelete) props.onDelete(password);
        } catch(err){ /* empty */ } finally{
            setIsLoaded(true);
        }
    }

    const handleSelect = (password : Password)=>{
        if(props?.onSelect) props.onSelect(password);
    }

    useEffect(()=>{
        setPasswords(props.passwords);
    }, [props.passwords])

    const transition = `transition-opacity ${isLoaded ? 'opacity-100' : 'opacity-0'} duration-700 ease-in-out`
    const trSelected = `border-b bg-slate-100 dark:bg-slate-950 dark:border-slate-700 ${transition}`
    const trNotSelected = `bg-white border-b hover:bg-slate-100 dark:hover:bg-slate-950 dark:bg-slate-900 dark:border-slate-700 ${transition}`
    
    return (
    <>
        <table className='w-full text-xs text-left text-gray-400'>
            <thead className='sticky top-[52px] text-xs text-gray-700 uppercase bg-slate-100 dark:bg-slate-950 dark:text-slate-300'>
                <tr>
                    <th className='px-6 py-3'>Nom</th>
                    <th className='px-6 py-3 hidden lg:table-cell'>Création</th>
                    <th className='px-6 py-3 hidden lg:table-cell'>Mis à jour</th>
                    <th className='px-6 py-3'></th>
                </tr>
            </thead>
            <tbody>
                {!isLoaded && <tr className={`bg-white border-b dark:bg-slate-900 dark:border-slate-700`}>
                    <td colSpan={4} className='px-6 py-2 text-center'>
                        <Spinner className="inline w-8 h-8 text-gray-200 dark:text-gray-600 fill-blue-600"/>
                        <span className="sr-only">Loading...</span>
                    </td>
                </tr>}
                {passwords && passwords.map((password : PasswordUI) => {
                    return <tr 
                        key={password.id} 
                        className={(password.isSelected) ? trSelected : trNotSelected}>
                    <td className={`px-6 py-2 font-medium flex gap-2 ${(password.isSelected) ? '' : 'cursor-pointer'}`} onClick={() => handleSelect(password)}>
                        <span><img src="https://placehold.co/60x40" alt="" /></span>
                        <div className='flex flex-col'>
                            <span className='text-base text-gray-900 dark:text-white'>{password.name}</span>
                            <small className={`text-xs italic ${transition}`}>{password.username}</small>
                        </div>
                    </td>
                    <td className='px-6 py-4 hidden lg:table-cell'>{moment(password.createdAt).fromNow()}</td>
                    <td className='px-6 py-4 hidden lg:table-cell'>{moment(password.updatedAt).fromNow()}</td>
                    <td className='px-6 py-4'>
                        <TrashIcon className="size-6 hover:text-red-400" onClick={()=> handleDelete(password)}/>
                    </td>
                </tr>
                })}
            </tbody>
        </table>
    </>
    
  )
}

export default PasswordsList