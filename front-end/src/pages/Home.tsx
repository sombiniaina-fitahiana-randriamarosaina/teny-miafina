import { MouseEvent, useEffect, useReducer, useState } from 'react'
import PasswordsList from '../components/PasswordsList'
import PasswordForm from '../components/PasswordForm'
import { Password } from '../services/models';
import { XMarkIcon } from '@heroicons/react/24/outline';
import { PlusIcon } from '@heroicons/react/16/solid';
import { passwordService } from '../services/password.service';
import { PasswordActionKind, passwordReducer } from '../reducers/Password.reducer';


function Home() {
    const [idPassword, setIdPassword] = useState<number | undefined>(undefined);
    const [isDarkMode, setIsDarkMode] = useState(true);
    const [isEditShown, setIsEditShown] = useState<boolean>(false);

    const [state, dispatch] = useReducer(passwordReducer, {passwords : []});

    // CRUD
    const handleFindAll = async () => {
        try {
            const response = await passwordService.getAll();
            dispatch({type : PasswordActionKind.REPLACE, payload : response.data.data});
        } catch(err){ /* empty */ } finally { /* empty */ }
    }

    const handleSave = async (password : Password, isUpdate : boolean) => {
        try {
            // await handleFindAll();
            if(isUpdate){
                dispatch({type : PasswordActionKind.UPDATE, payload : [password]})
            } else {
                dispatch({type : PasswordActionKind.ADD, payload : [password]})
            }
            setIsEditShown(!isEditShown);
            setIdPassword(undefined);
        } catch(err){ /* empty */ }
    }

    const handleDelete = (password : Password) => {
        dispatch({type : PasswordActionKind.REMOVE, payload : [password]})
    }



    const handleAddPassword = ()=>{
        onPasswordSelected(undefined);
    }

    const onPasswordSelected = (password : Password | undefined)=>{
        setIdPassword(password?.id);
        if(password) dispatch({type : PasswordActionKind.SELECT, payload : [password]})
        setIsEditShown(true);
    }

    const handleModeChange = () => {
        setIsDarkMode(prevState => !prevState);
    }

    const onCloseEditor = (e : MouseEvent<HTMLButtonElement>) =>{
        e.preventDefault();
        dispatch({type : PasswordActionKind.RESET_SELECTION, payload : []})
        setIsEditShown(false);
    }

    useEffect(()=>{
        handleFindAll();
    }, [])
    
    return <>
        <div className={`w-full ${isDarkMode ? 'dark' : ''}`}>
            <div className='flex dark:bg-slate-900 min-h-screen'>
                <div className={`flex flex-col ${isEditShown ? 'max-md:hidden max-lg:w-1/2 max-xl:w-3/5 w-3/4' : 'w-full'} dark:bg-slate-900`}>
                    <div className="sticky top-0 flex justify-between bg-white dark:bg-slate-900 p-2">
                        <label className="w-24 inline-flex items-center cursor-pointer">
                            <input type="checkbox" className="sr-only peer" onChange={handleModeChange} checked={isDarkMode} />
                            <div className="relative w-11 h-6 bg-slate-300 peer-focus:outline-none rounded-full peer peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-slate-600"></div>
                            <span className="ms-3 text-sm font-medium text-gray-900 dark:text-gray-300">Dark</span>
                        </label>
                        <button className='max-w-28 text-sm px-4 py-2 rounded text-slate-50 bg-blue-800 hover:bg-blue-900 flex items-center justify-center' onClick={handleAddPassword}>
                            <PlusIcon className='size-5'/>
                            Ajouter
                        </button>
                    </div>
                    <PasswordsList passwords={state.passwords} onSelect={onPasswordSelected} onDelete={handleDelete}/>
                </div>
                <div className={`${isEditShown ? 'w-full max-h-screen sticky top-0 md:w-1/2 lg:w-2/5 xl:w-1/4' : 'hidden'} border-l bg-slate-100 dark:border-slate-800 shadow-md dark:shadow-slate-800 dark:bg-slate-800 animate-slideInRight transition`}>
                    <div className='flex w-full justify-end p-2'>
                        <button onClick={onCloseEditor}>
                            <XMarkIcon className='size-6 hover:scale-110 dark:text-white'/>
                        </button>
                    </div>
                    <PasswordForm idPassword={idPassword} onCancel={onCloseEditor} onSave={handleSave}/>
                </div>
            </div>
        </div>
    </>
}

export default Home

