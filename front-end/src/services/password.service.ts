import { AxiosResponse } from "axios" 
import axiosInstance from "./axiosInstance";
import { Response, Password, Success } from "./models";

// const delay = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export const passwordService = {
    getAll : () : Promise<AxiosResponse<Response<Password[]>>> => {
        return axiosInstance.get<Response<Password[]>>('/passwords');
    },
    getOne : async (id : number | undefined) : Promise<AxiosResponse<Response<Password>>> => {
        // await delay(1000)
        return axiosInstance.get<Response<Password>>(`/passwords/${id}`);
    },
    delete : (id : number | undefined) : Promise<AxiosResponse<Response<Success>>> => {
        return axiosInstance.delete<Response<Success>>(`/passwords/${id}`);
    },
    update : async (id : number | undefined, password : Password) : Promise<AxiosResponse<Response<Password>>> => {
        return axiosInstance.put<Response<Password>>(`/passwords/${id}`, password);
    },
    create : (password : Password) : Promise<AxiosResponse<Response<Password>>> => {
        return axiosInstance.post<Response<Password>>(`/passwords`, password);
    }
}