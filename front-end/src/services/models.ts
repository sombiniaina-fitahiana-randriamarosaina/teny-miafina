export interface Response<T>{
    timestamp : Date,
    status : number,
    data : T
}

export interface Password {
    id?: number;
    name: string;
    username: string;
    password: string;
    createdAt?: Date;
    updatedAt?: Date;
}

export interface PasswordUI extends Password{
    isSelected : boolean
}

export interface Success {
    message: string;
}
