import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: import.meta.env.VITE_URL_API,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
  },
});

// Intercepteurs de requête
axiosInstance.interceptors.request.use(
  config => {
    // config.headers["Authorization"] = "Basic Test";
    return config;
  },
  error => Promise.reject(error)
);

// Intercepteurs de réponse
axiosInstance.interceptors.response.use(
  response => response,
  error => {
    return Promise.reject(error);
  }
);

export default axiosInstance;
