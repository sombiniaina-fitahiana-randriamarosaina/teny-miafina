import { Password, PasswordUI } from "../services/models";

export enum PasswordActionKind {
    REPLACE = 'REPLACE',
    ADD = 'ADD',
    UPDATE = 'UPDATE',
    REMOVE = 'REMOVE',
    SELECT = 'SELECT',
    RESET_SELECTION = 'RESET'
}

interface PasswordAction {
    type : PasswordActionKind;
    payload : Password[]
}

interface PasswordState {
    passwords : PasswordUI[]
}

export function passwordReducer(state : PasswordState, action : PasswordAction) : PasswordState{
    const {type, payload} = action;

    switch(type){
        case PasswordActionKind.REPLACE : {
            return { 
                ...state,
                passwords: payload.map((p) => ({...p, isSelected : false})) 
            };
        }
        
        case PasswordActionKind.SELECT : {
            const passwords = state.passwords.map((password) => {
                const test = payload.some((p) => p.id === password.id);
                return (test) ? {...password, isSelected : true} : {...password, isSelected : false}
            })
            return {
                ...state, 
                passwords : passwords
            }

        }

        case PasswordActionKind.RESET_SELECTION : {
            return {
                ...state, 
                passwords : state.passwords.map((p) => ({...p, isSelected : false}))
            }
        }

        case PasswordActionKind.ADD : {
            return {
                ...state, 
                passwords : [
                    ...payload.map((p) => ({...p, isSelected : false})),
                    ...state.passwords, 
                ]
            }
        }

        case PasswordActionKind.UPDATE : {
            return {
                ...state, 
                passwords : [
                    ...state.passwords.map((password) => {
                        const passwordFound = payload.find((p) => p.id === password.id);
                        return passwordFound ? { ...passwordFound, isSelected: false } : password;
                    })
                ]
            }
        }

        case PasswordActionKind.REMOVE : {
            return {
                ...state, 
                passwords : state.passwords.filter((password) => !payload.some((p)=> p.id === password.id))
            }
        }

        default :
            return state
    }
}