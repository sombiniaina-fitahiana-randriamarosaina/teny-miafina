# Password Manager

## Description

Teny Miafina est une application sécurisée permettant de stocker, gérer et générer des mots de passe pour divers comptes en ligne. Cette application assure la sécurité et la confidentialité de vos mots de passe grâce à des techniques de cryptage avancées. Actuellement, cette application est à usage personnel.


## Fonctionnalités

- Stockage sécurisé des mots de passe

## Technologies utilisées

- **Back-end** :
  - **Langage de programmation** : Java 17
  - **Framework** : Spring Boot 3.3.0

- **Front-end** :
  - **Langage de programmation** : TypeScript
  - **Framework** : React

- **Base de données** : MariaDB

- **Développement**
  - **IDE** : Intellij IDEA
- **Production**
  - **Docker**

## Installation 

#### Prérequis
+ Assurez-vous d'avoir Java 17, Node, Docker et un IDE (j'utilise Intellij IDEA et visual studio code) installés sur votre machine

#### Étapes d'installation

1. **Clonez le dépôt**

   ```bash
   $ git clone https://gitlab.com/sombiniaina-fitahiana-randriamarosaina/teny-miafina.git
   ```
2. **Installation des dependances**
   ```bash
    $ cd front-end && npm i
    ```
3. **Dans le dossier front-end**
   - Creer un fichier d'environnement local `.env.local`
   ```bash
    VITE_URL_API=http://localhost:8080/api
    ```

4. **Lancer Docker**

5. **Lancez la base de donnée sur Docker**
    Cette commande lance les conteneurs sur Docker
    ```bash
    $ ./run_bdd.sh
    ```
6. **Lancer les 2 Projets**
    - Front-end :
   ```bash
    $ cd front-end && npm run dev
    ```
    - Back-end : Lancer back-end sur votre IDE
  
7. **Creez la base de données et les tables sur MariaDB Dans Docker**
   
   le script est dans le fichier `back-end/tenymiafina.sql`

8. **Accédez à l'application**
   - **Le back-end** : API accessible via http://127.0.0.1:8080
   - **Le front-end** : API accessible via http://127.0.0.1:5173


## Auteurs
- **Randriamarosaina Sombiniaina Fitahiana**