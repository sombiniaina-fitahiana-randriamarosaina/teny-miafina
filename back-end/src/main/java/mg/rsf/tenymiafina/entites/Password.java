package mg.rsf.tenymiafina.entites;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.generator.EventType;

import java.time.ZonedDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
@ToString
@Table(name = "PASSWORD")
public class Password {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PASSWORD")
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @org.hibernate.annotations.Generated
    @Column(name = "CREATED_AT", insertable = false, updatable = false)
    private ZonedDateTime createdAt;

    @org.hibernate.annotations.Generated(event = {EventType.INSERT, EventType.UPDATE})
    @Column(name = "UPDATED_AT", insertable = false, updatable = false)
    private ZonedDateTime updatedAt;
}
