package mg.rsf.tenymiafina.repositories;

import mg.rsf.tenymiafina.entites.Password;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PasswordRepository extends JpaRepository<Password, Integer> {
    Password findByName(String name);
    @Query("SELECT p FROM Password p WHERE p.name = ?1 AND p.id != ?2")
    Password findByNameAndIdNot(String name, int id);
}
