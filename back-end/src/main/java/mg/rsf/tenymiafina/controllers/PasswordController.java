package mg.rsf.tenymiafina.controllers;

import mg.rsf.tenymiafina.entites.Password;
import mg.rsf.tenymiafina.services.PasswordService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "passwords")
public class PasswordController {
    private PasswordService passwordService;

    public PasswordController(PasswordService passwordService) {
        this.passwordService = passwordService;
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping
    public Password create(@RequestBody Password password){
        return this.passwordService.save(password);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<Password> findAll(){
        return this.passwordService.findAll();
    }

    @GetMapping(path = "{id}", produces = APPLICATION_JSON_VALUE)
    public Password find(@PathVariable int id){
        return this.passwordService.findById(id);
    }

    @ResponseStatus(value = HttpStatus.ACCEPTED)
    @DeleteMapping(path = "{id}")
    public Map<String,String> deleteById(@PathVariable int id){
        this.passwordService.deleteById(id);
        return Map.of("message", "OK");
    }

    @ResponseStatus(value = HttpStatus.ACCEPTED)
    @PutMapping(path = "{id}")
    public Password update(@PathVariable int id, @RequestBody Password password){
        return this.passwordService.updateById(id, password);
    }
}
