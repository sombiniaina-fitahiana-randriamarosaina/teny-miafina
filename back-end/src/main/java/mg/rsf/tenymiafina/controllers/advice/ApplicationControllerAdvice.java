package mg.rsf.tenymiafina.controllers.advice;

import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import mg.rsf.tenymiafina.dto.ErrorEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ApplicationControllerAdvice {
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({EntityExistsException.class})
    public @ResponseBody ErrorEntity handleEntityExistsException(EntityExistsException e){
        return new ErrorEntity("400", e.getMessage());
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler({EntityNotFoundException.class})
    public @ResponseBody ErrorEntity handleEntityExistsException(EntityNotFoundException e){
        return new ErrorEntity("404", e.getMessage());
    }
}
