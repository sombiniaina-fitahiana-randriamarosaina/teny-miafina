package mg.rsf.tenymiafina.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;

import java.util.Date;

public record ApiResponseDTO<T>(
        Date timestamp,
        int status,
        T data
) {
    public String toJson() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to convert ApiResponseDTO to JSON", e);
        }
    }
}
