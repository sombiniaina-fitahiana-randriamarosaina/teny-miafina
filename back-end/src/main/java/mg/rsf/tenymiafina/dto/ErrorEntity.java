package mg.rsf.tenymiafina.dto;

public record ErrorEntity(
        String code,
        String message
) {
}
