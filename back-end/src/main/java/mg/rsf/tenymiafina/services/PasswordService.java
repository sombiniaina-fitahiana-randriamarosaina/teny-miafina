package mg.rsf.tenymiafina.services;

import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import mg.rsf.tenymiafina.entites.Password;
import mg.rsf.tenymiafina.repositories.PasswordRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Service
public class PasswordService {
    private PasswordRepository passwordRepository;

    public PasswordService(PasswordRepository passwordRepository) {
        this.passwordRepository = passwordRepository;
    }

    public Password save(Password password){
        Password passwordFound = passwordRepository.findByNameAndIdNot(password.getName(), password.getId());
        if(passwordFound != null) throw new EntityExistsException(String.format("Un enregistrement de même nom existe déjà"));
        return this.passwordRepository.save(password);
    }

    public List<Password> findAll(){
        return this.passwordRepository.findAll();
    }

    public Password findById(int id){
        Optional<Password> optionalPassword = this.passwordRepository.findById(id);
        if(optionalPassword.isEmpty()) throw new EntityNotFoundException(String.format("L'enregistrement avec l'id : %d n'a pas été trouvé", id));
        return optionalPassword.get();
    }

    public void deleteById(int id){
        Password password = this.findById(id);
        this.passwordRepository.delete(password);
    }

    public Password updateById(int id, Password password){
        Password passwordFound = this.findById(id);
        passwordFound.setName(password.getName());
        passwordFound.setUsername(password.getUsername());
        passwordFound.setPassword(password.getPassword());
        return this.save(passwordFound);
    }
}
